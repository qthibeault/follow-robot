#include <getopt.h>

#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>

int videoDevice = 0;
float markerSize = 75.0;
float threshold = 25.0;
std::string calibrationFilename = "calibration.yml";
std::string serialDevice = "/dev/ttyUSB0";

constexpr struct option long_options[] = {
    { "marker-size", required_argument, NULL, 'm' },
    { "video-device", required_argument, NULL, 'v' },
    { "config", required_argument, NULL, 'c' },
    { "threshold", required_argument, NULL, 't' },
    { "serial-device", required_argument, NULL, 's' },
    { NULL, 0, NULL, 0 }
};

int
main(int argc, char* argv[])
{
    /* Do not print errors */
    opterr = 0;
    /* Parse options */
    int opt;
    while ((opt = getopt_long(argc, argv, "m:v:c:t:s:", long_options, NULL)) !=
           -1) {
        switch (opt) {
            case 'd': {
                std::string argval(optarg);
                videoDevice = std::stoi(argval);
                break;
            }

            case 'm': {
                std::string argval(optarg);
                markerSize = std::stof(argval);
                break;
            }

            case 'c': {
                calibrationFilename = std::string(optarg);
                break;
            }

            case 't': {
                std::string argval(optarg);
                threshold = std::stof(argval);
                break;
            }

            case 's': {
                serialDevice = std::string(optarg);
                break;
            }

            case ':':
            case '?':
            default:
                std::cerr << "Invalid option!" << std::endl
                          << "Usage: calibrate [--video-device <dev number>"
                          << "--marker-size <length in mm>] --device <number> "
                             "--config <filename> --threshold <number> "
                             "--serial-device <filename>"
                          << std::endl;
                return 1;
        }
    }

    std::cout << "Ready to capture marker " << std::endl
              << "Press [Enter] when marker is in frame" << std::endl;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cv::Mat frame;
    std::vector<int> ids;
    cv::VideoCapture video(videoDevice);
    std::vector<std::vector<cv::Point2f>> corners;
    cv::Ptr<cv::aruco::Dictionary> dictionary =
      cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);

    while (ids.size() != 1) {
        video.read(frame);
        cv::aruco::detectMarkers(frame, dictionary, corners, ids);

        cv::waitKey(25);
    }

    int targetId = ids.at(0);
    std::cout << "Locked marker with id: " << targetId << std::endl;

    cv::Mat cameraMatrix, distCoeffs;
    cv::FileStorage storage(calibrationFilename, cv::FileStorage::READ);
    storage["cameraMatrix"] >> cameraMatrix;
    storage["distCoeffs"] >> distCoeffs;
    storage.release();

#ifndef NDEBUG
    cv::namedWindow("Debug", cv::WINDOW_AUTOSIZE);
#endif

    while (video.read(frame)) {
        cv::Mat gray_frame;
        cv::cvtColor(frame, gray_frame, cv::COLOR_BGR2GRAY);

        cv::aruco::detectMarkers(gray_frame, dictionary, corners, ids);
        for (auto i = 0; i < ids.size(); ++i) {
            if (ids.at(i) == targetId) {
                std::vector<std::vector<cv::Point2f>> targetCorners;
                std::vector<int> targetId;

                targetCorners.push_back(corners.at(i));
                targetId.push_back(ids.at(i));

                std::vector<cv::Vec3d> rvecs, tvecs;
                cv::aruco::estimatePoseSingleMarkers(targetCorners,
                                                     markerSize,
                                                     cameraMatrix,
                                                     distCoeffs,
                                                     rvecs,
                                                     tvecs);

                // WIP: Determine direction to turn to center marker
                assert(tvecs.size() == 1);
                cv::Vec3d tvec = tvecs.front();

                if (abs(tvec[0]) > threshold) {
                    std::cout << "["
                              << std::chrono::system_clock::to_time_t(
                                   std::chrono::system_clock::now())
                              << "] Delta: " << tvec[0] << " Direction: ";

                    if (tvec[0] > 0) {
                        std::cout << "right" << std::endl;
                    } else {
                        std::cout << "Left" << std::endl;
                    }
                }

#ifndef NDEBUG
                cv::aruco::drawDetectedMarkers(frame, targetCorners, targetId);

                for (auto i = 0; i < targetId.size(); ++i) {
                    cv::aruco::drawAxis(frame,
                                        cameraMatrix,
                                        distCoeffs,
                                        rvecs.at(i),
                                        tvecs.at(i),
                                        75);
                }
#endif
            }
        }

#ifndef NDEBUG
        cv::imshow("Debug", frame);
#endif
        cv::waitKey(100);
    }

    video.release();
    return 0;
}