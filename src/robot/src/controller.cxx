#include <boost/asio.hpp>
#include <boost/assert.hpp>
#include <boost/log/trivial.hpp>
#include <boost/program_options.hpp>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <signal.h>
#include <unistd.h>
#include <vector>

#define IROBOT_DEFAULT_BAUD_RATE 115200
#define IROBOT_DEFAULT_CHAR_SIZE 8

typedef unsigned char byte_t;

volatile sig_atomic_t stopFlagRaised = 0;

void
handler(int signal)
{
    stopFlagRaised = 1;
}

size_t
writeCommand(std::vector<byte_t> bytes, boost::asio::serial_port& port)
{
    size_t count = 0;
    unsigned char command[1] = { 0 };

    for (const byte_t byte : bytes) {
        command[0] = static_cast<unsigned char>(byte);
        count += boost::asio::write(port, boost::asio::buffer(command, 1));
    }

    return count;
}

int
main(int argc, char* argv[])
{
    float markerSize, threshold;
    bool invertFlag, graphicsFlag;
    std::string calibrationFile, videoDevice, serialDevice;

    using namespace boost;

    /* Parse command line configuration options */
    program_options::options_description desc("Allowed options");
    /* clang-format off */
    desc.add_options()
        ("help", "produce help message")
        ("invert", program_options::bool_switch(&invertFlag)->default_value(false), "Invert image vertically")
        ("graphics", program_options::bool_switch(&graphicsFlag)->default_value(false), "Show graphics output")
        ("markerSize", program_options::value<float>(&markerSize)->default_value(100.0), "Set marker size in mm")
        ("videoDevice", program_options::value<std::string>(&videoDevice)->default_value("/dev/video0"), "Set video device")
        ("calibrationFile", program_options::value<std::string>(&calibrationFile)->default_value("calibration.yml"), "File containing camera calibration")
        ("serialDevice", program_options::value<std::string>(&serialDevice)->default_value("/dev/ttyUSB0"), "Set serial communication device")
        ("threshold", program_options::value<float>(&threshold)->default_value(25.0), "Set deadzone threshold");
    /* clang-format on */

    program_options::variables_map vm;
    program_options::store(
      program_options::parse_command_line(argc, argv, desc), vm);
    program_options::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    // Log configuration
    BOOST_LOG_TRIVIAL(info) << "Graphics\t\t" << graphicsFlag;
    BOOST_LOG_TRIVIAL(info) << "Marker Size\t\t" << markerSize;
    BOOST_LOG_TRIVIAL(info) << "Video Device\t" << videoDevice;
    BOOST_LOG_TRIVIAL(info) << "Calibration File\t" << calibrationFile;
    BOOST_LOG_TRIVIAL(info) << "Serial Device\t" << serialDevice;
    BOOST_LOG_TRIVIAL(info) << "Threshold\t\t" << threshold << std::endl;

    // Read calibration data from provided file
    cv::FileStorage calibration;
    cv::Mat cameraMatrix, distCoeffs;

    try {
        calibration.open(calibrationFile, cv::FileStorage::READ);
        calibration["cameraMatrix"] >> cameraMatrix;
        calibration["distCoeffs"] >> distCoeffs;
        calibration.release();
    } catch (const cv::Exception& e) {
        BOOST_LOG_TRIVIAL(error) << "Could not read calibration file";
        return EXIT_FAILURE;
    }

    // Setup camera
    cv::VideoCapture capture;

    try {
        capture.open(videoDevice);
    } catch (const cv::Exception& e) {
        BOOST_LOG_TRIVIAL(error) << "Could not open video capture";
        return EXIT_FAILURE;
    }

    BOOST_ASSERT_MSG(capture.isOpened(), "Expected capture to be open");

    BOOST_LOG_TRIVIAL(info) << "Ready to capture target marker";
    BOOST_LOG_TRIVIAL(info) << "Press [Enter] to continue...";

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    // register exit signal handler
    signal(SIGINT, handler);

    if (graphicsFlag)
        cv::namedWindow("debug", cv::WINDOW_AUTOSIZE);

    // Setup serial port to communicate with iRobot create
    asio::io_service io;
    asio::serial_port serialPort(io);

    try {
        serialPort.open(serialDevice);
        serialPort.set_option(
          asio::serial_port_base::baud_rate(IROBOT_DEFAULT_BAUD_RATE));
        serialPort.set_option(
          asio::serial_port_base::character_size(IROBOT_DEFAULT_CHAR_SIZE));
        serialPort.set_option(
          asio::serial_port_base::parity(asio::serial_port_base::parity::none));
        serialPort.set_option(asio::serial_port_base::flow_control(
          asio::serial_port_base::flow_control::none));
        serialPort.set_option(asio::serial_port_base::stop_bits(
          asio::serial_port_base::stop_bits::one));
    } catch (const system::system_error& e) {
        BOOST_LOG_TRIVIAL(error) << "Could not open serial device";
        capture.release();
        return EXIT_FAILURE;
    }

    BOOST_ASSERT_MSG(serialPort.is_open(), "Expected serial port to be open");

    // Capture target marker
    cv::Mat image, grayImage;
    std::vector<int> capturedIds;
    std::vector<std::vector<cv::Point2f>> corners;
    cv::Ptr<cv::aruco::Dictionary> dictionary =
      cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);

    while (!stopFlagRaised && capturedIds.size() != 1) {
        capture >> image;
        if (invertFlag) {
            cv::Mat tmpImage = image.clone();
            cv::flip(tmpImage, image, 0);
        }

        cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
        cv::aruco::detectMarkers(grayImage, dictionary, corners, capturedIds);

        if (graphicsFlag) {
            cv::aruco::drawDetectedMarkers(image, corners, capturedIds);
            cv::imshow("debug", image);
        }

        usleep(25000);
    }

    int targetMarkerId = capturedIds.front();

    BOOST_LOG_TRIVIAL(info) << "Target marker id\t" << targetMarkerId;

    // Write start and mode command to iRobot
    std::vector<byte_t> command{ 128, 131 };
    std::size_t bytes = writeCommand(command, serialPort);

    BOOST_ASSERT_MSG(bytes == 2, "Expected to transfer 2 bytes");

    // Begin main logic loop
    while (!stopFlagRaised && capture.read(image)) {
        if (invertFlag) {
            cv::Mat tmpImage = image.clone();
            cv::flip(tmpImage, image, 0);
        }

        cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
        cv::aruco::detectMarkers(grayImage, dictionary, corners, capturedIds);

        if (graphicsFlag)
            cv::aruco::drawDetectedMarkers(image, corners, capturedIds);

        for (auto i = 0; i < capturedIds.size(); ++i) {
            if (capturedIds.at(i) == targetMarkerId) {
                command = { 137, 0, 0, 128, 0 };

                bytes = writeCommand(command, serialPort);
                BOOST_ASSERT_MSG(bytes == 5, "Expected to send 5 bytes");

                std::vector<cv::Vec3d> rvecs, tvecs;
                std::vector<std::vector<cv::Point2f>> targetMarkerCorners =
                  std::vector<std::vector<cv::Point2f>>(
                    corners.begin() + i, corners.begin() + i + 1);

                cv::aruco::estimatePoseSingleMarkers(targetMarkerCorners,
                                                     markerSize,
                                                     cameraMatrix,
                                                     distCoeffs,
                                                     rvecs,
                                                     tvecs);

                BOOST_ASSERT_MSG(tvecs.size() == 1,
                                 "Only expected to detect 1 marker");

                if (graphicsFlag)
                    cv::aruco::drawAxis(image,
                                        cameraMatrix,
                                        distCoeffs,
                                        rvecs.front(),
                                        tvecs.front(),
                                        75);

                float xOffset = tvecs.front()[0];
                if (abs(xOffset) > threshold) {
                    if (xOffset < 0) {
                        // Turn right
                        BOOST_LOG_TRIVIAL(debug) << "Turning right";
                        command = { 137, 0, 200, 255, 255 };
                    } else {
                        // Turn left
                        BOOST_LOG_TRIVIAL(debug) << "Turning left";
                        command = { 137, 0, 200, 0, 1 };
                    }
                } else {
                    command = { 137, 0, 0, 128, 0 };
                }

                bytes = writeCommand(command, serialPort);
                BOOST_ASSERT_MSG(bytes == 5, "Expected to transfer 5 bytes");

                break;
            }
        }

        if (graphicsFlag)
            cv::imshow("debug", image);

        usleep(20000);
    }

    if (graphicsFlag)
        cv::destroyAllWindows();

    command = { 173 };
    bytes = writeCommand(command, serialPort);

    BOOST_ASSERT_MSG(bytes == 1, "Expected to write 1 byte");

    serialPort.close();
    capture.release();

    return EXIT_SUCCESS;
}