#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <limits>
#include <string>

#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>

int main(int argc, char *argv[]) {
#ifndef NDEBUG
    std::cout << "Running in Debug configuration" << std::endl;
#endif

    /* sensible defaults */
    int imageCount = 10;
    int boardWidth = 6;
    int boardHeight = 7;
    int videoDevice = 0;
    float squareSize = 20.0;

    /* Setup available options */
    const static struct option long_options[] = {
        {"width", required_argument, NULL, 'w'},
        {"height", required_argument, NULL, 'h'},
        {"count", required_argument, NULL, 'c'},
        {"device", required_argument, NULL, 'd'},
        {"square-size", required_argument, NULL, 's'},
        {NULL, 0, NULL, 0}};

    /* Do not print errors */
    opterr = 0;

    /* Parse options */
    int opt;
    while ((opt = getopt_long(argc, argv, "w:h:c:d:s:", long_options, NULL)) !=
           -1) {
        switch (opt) {
        case 'w': {
            std::string argval(optarg);
            boardWidth = std::stoi(argval);
            break;
        }

        case 'h': {
            std::string argval(optarg);
            boardHeight = std::stoi(argval);
            break;
        }

        case 'c': {
            std::string argval(optarg);
            imageCount = std::stoi(argval);
            break;
        }

        case 'd': {
            std::string argval(optarg);
            videoDevice = std::stoi(argval);
            break;
        }

        case 's': {
            std::string argval(optarg);
            squareSize = std::stof(argval);
            break;
        }

        case ':':
        case '?':
        default:
            std::cerr << "Invalid option!" << std::endl;
            std::cerr << "Usage: calibrate [--count <count> --height <height> "
                         "--width <width>]"
                      << std::endl;
            return 1;
        }
    }

    std::cout << "Preparing to calibrate camera" << std::endl
              << "Height: " << boardHeight << "\tWidth: " << boardWidth
              << "\tCount: " << imageCount << std::endl
              << "Press [Enter] to continue..." << std::endl;

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

#ifndef NDEBUG
    cv::namedWindow("View", cv::WINDOW_AUTOSIZE);
#endif

    cv::Mat frame;
    cv::Size boardSize(boardWidth, boardHeight);
    cv::VideoCapture capture(videoDevice);

    std::vector<std::vector<cv::Point2f>> imagePoints;
    std::vector<std::vector<cv::Point3f>> objectPoints;

    int foundCount = 0;
    while (foundCount < imageCount) {
        capture.read(frame);
        std::vector<cv::Point2f> points;

        bool found = cv::findChessboardCorners(
            frame, boardSize, points,
            cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_NORMALIZE_IMAGE);

        if (found) {
            std::cout << "Found " << ++foundCount << "\\" << imageCount
                      << std::endl;

            cv::Mat grayFrame;
            cv::cvtColor(frame, grayFrame, cv::COLOR_BGR2GRAY);
            cv::cornerSubPix(grayFrame, points, cv::Size(11, 11),
                             cv::Size(-1, -1),
                             cv::TermCriteria(cv::TermCriteria::EPS +
                                                  cv::TermCriteria::COUNT,
                                              30, 0.0001));

#ifndef NDEBUG
            cv::drawChessboardCorners(frame, boardSize, cv::Mat(points), found);
#endif

            std::vector<cv::Point3f> object;
            for (auto i = 0; i < boardHeight; ++i)
                for (auto j = 0; j < boardWidth; ++j)
                    object.push_back(cv::Point3f((float)j * squareSize,
                                                 (float)i * squareSize, 0));

            imagePoints.push_back(points);
            objectPoints.push_back(object);
        }

#ifndef NDEBUG
        cv::imshow("View", frame);
#endif
        cv::waitKey(25);
    }

    capture.release();
    cv::destroyAllWindows();

    cv::Mat cameraMatrix, distCoeffs;
    std::vector<cv::Mat> rVectors, tVectors;
    cv::calibrateCamera(objectPoints, imagePoints, frame.size(), cameraMatrix,
                        distCoeffs, rVectors, tVectors, 0);

    cv::FileStorage file("calibration.yml", cv::FileStorage::WRITE);
    file << "cameraMatrix" << cameraMatrix;
    file << "distCoeffs" << distCoeffs;

    file.release();

    return 0;
}